package com.todo.demo.model;

import com.todo.demo.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Utilisateur extends DateAudit {

    @Id
    private String utilisateurname;

    private String password;

    @ManyToMany
    private Set<Role> roles = new HashSet<>();

    public Utilisateur() {}

    public Utilisateur(String utilisateurname, String password) {
        this.utilisateurname = utilisateurname;
        this.password = password;
    }

    public String getUtilisateurname() {
        return utilisateurname;
    }

    public void setUtilisateurname(String utilisateurname) {
        this.utilisateurname = utilisateurname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "utilisateurname='" + utilisateurname + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                '}';
    }
}
