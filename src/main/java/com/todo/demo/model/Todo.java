package com.todo.demo.model;

import com.todo.demo.model.audit.UserDateAudit;

import javax.persistence.*;

@Entity
public class Todo extends UserDateAudit {

    @Id
    @GeneratedValue
    private int id;

    private String titre;
    private String desciption;

    @ManyToOne
    private Etat etat;

    public Todo() {}

    public Todo(String titre, String desciption, Etat etat, Utilisateur user) {
        this.titre = titre;
        this.desciption = desciption;
        this.etat = etat;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }


    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getDesciption() {
        return desciption;
    }

    public Etat getEtat() {
        return etat;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", desciption='" + desciption + '\'' +
                ", etat='" + etat + '\'' +
                '}';
    }
}
