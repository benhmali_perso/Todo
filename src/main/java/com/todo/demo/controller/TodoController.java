package com.todo.demo.controller;

import com.todo.demo.dao.TodoDao;
import com.todo.demo.dao.UserDao;
import com.todo.demo.model.Todo;
import com.todo.demo.payload.ApiResponse;
import com.todo.demo.payload.TodoRequest;
import com.todo.demo.service.TodoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Api(description = "Gestion des tâches")
@RestController
public class TodoController {

    @Autowired
    private TodoDao todoDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TodoService todoService;

    //Todos
    @PostMapping(value = "/Todo")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<?> addTodo(@Valid @RequestBody TodoRequest todoRequest) {

        Todo todo = todoService.createTodo(todoRequest);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(todo.getId())
                .toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true,"Todo created successfully"));


    }

    /*//Todos/{user}
    @GetMapping(value = "/Todo/{userLastName}")
    public List<Todo> getTodoByUser(@PathVariable String userLastName) {
        User user = userDao.findById(userLastName).get();
        List<Todo> todos = todoDao.findByUser(user);
        return todos;
    }*/

    //Todos/{id}
    @DeleteMapping(value = "/Todo/{id}")
    public ResponseEntity<String> deleteTodo(@PathVariable int id) {
        Todo todo = todoDao.getOne(id);
        todoDao.delete(todo);
        return new ResponseEntity<>("Tâche supprimée", HttpStatus.OK);

    }
}
