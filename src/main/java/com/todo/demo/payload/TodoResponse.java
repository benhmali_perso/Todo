package com.todo.demo.payload;

import com.todo.demo.model.Etat;

import java.time.Instant;

public class TodoResponse {
    private int id;
    private String titre;
    private String description;
    private Etat etat;
    private UserSummary createdBy;

    public TodoResponse(int id, String titre, String description, Etat etat, UserSummary createdBy) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.etat = etat;
        this.createdBy = createdBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public UserSummary getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserSummary createdBy) {
        this.createdBy = createdBy;
    }
}
