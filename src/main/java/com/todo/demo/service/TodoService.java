package com.todo.demo.service;

import com.todo.demo.dao.EtatDao;
import com.todo.demo.dao.TodoDao;
import com.todo.demo.dao.UserDao;
import com.todo.demo.exception.BadRequestException;
import com.todo.demo.exception.ResourceNotFoundException;
import com.todo.demo.model.Todo;
import com.todo.demo.model.Utilisateur;
import com.todo.demo.payload.PagedResponse;
import com.todo.demo.payload.TodoRequest;
import com.todo.demo.payload.TodoResponse;
import com.todo.demo.payload.UserSummary;
import com.todo.demo.security.UserPrincipal;
import com.todo.demo.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class TodoService {
    @Autowired
    TodoDao todoDao;

    @Autowired
    UserDao userDao;

    @Autowired
    EtatDao etatDao;

    private final static Logger logger = LoggerFactory.getLogger(TodoService.class);

    private void validatePageNumberAndSize(int page, int size) {
        if (page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if (size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
    }


    public Todo createTodo(TodoRequest todoRequest) {
        Todo todo = new Todo();
        todo.setTitre(todoRequest.getTitre());
        todo.setDesciption(todoRequest.getDescription());
        todo.setEtat(etatDao.findByDescription(todoRequest.getEtat().getDescription()));

        return todoDao.save(todo);
    }


    public PagedResponse<TodoResponse> getTodosByUser(String username, UserPrincipal currentUser, int page, int size) {
        validatePageNumberAndSize(page, size);

        Utilisateur utilisateur = userDao.findById(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
        Page<Todo> todos = todoDao.findByCreatedBy(username, pageable);

        if (todos.getNumberOfElements() == 0){
            return new PagedResponse<>(Collections.emptyList(), todos.getNumber(),
                    todos.getSize(), todos.getTotalElements(), todos.getTotalPages(), todos.isLast());
        }

        String pattern = "MM-dd-yyyy HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        List<TodoResponse> todosResponse = todos.map(todo -> {
            Date date = Date.from(todo.getCreatedAt());
            String formattedDate = simpleDateFormat.format(date);
            return new TodoResponse(todo.getId(), todo.getTitre(), todo.getDesciption(), todo.getEtat(), new UserSummary(utilisateur.getUtilisateurname(), formattedDate));
        }).getContent();

        return new PagedResponse<>(todosResponse, todos.getNumber(),
                todos.getSize(), todos.getTotalElements(), todos.getTotalPages(), todos.isLast());
    }
}
