package com.todo.demo.security;

import com.todo.demo.dao.UserDao;
import com.todo.demo.model.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserDao userDao;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur utilisateur = userDao.findById(username)
                .orElseThrow(() ->
                        new UsernameNotFoundException("Usernot found")
                );
        return UserPrincipal.create(utilisateur);
    }

    @Transactional
    public UserDetails loadUserById(String id) {
        Utilisateur utilisateur = userDao.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );

        return UserPrincipal.create(utilisateur);
    }
}
