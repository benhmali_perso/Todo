package com.todo.demo.dao;

import com.todo.demo.model.Etat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtatDao extends JpaRepository<Etat, Integer> {
    <Optional>Etat findByDescription(String description);
}
