package com.todo.demo.dao;

import com.todo.demo.model.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoDao extends JpaRepository<Todo, Integer> {

    Page<Todo> findByCreatedBy(String username, Pageable pageable);
}
