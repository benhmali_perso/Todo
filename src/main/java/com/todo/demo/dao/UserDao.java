package com.todo.demo.dao;

import com.todo.demo.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<Utilisateur, String> {
}
